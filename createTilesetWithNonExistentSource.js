#!/usr/bin/env node

const MapboxTilesets = require('@mapbox/mapbox-sdk/services/tilesets.js')
const tilesetsService = MapboxTilesets({ accessToken: process.env.MAPBOX_ACCESS_TOKEN })

const parseToken = require('@mapbox/parse-mapbox-token')
const token = parseToken(process.env.MAPBOX_ACCESS_TOKEN)

tilesetsService.createTileset({
    tilesetId: `${token.user}.tileset-without-source`,
    recipe: {
      version: 1,
      layers: {
        my_layer: {
          source: `mapbox://tileset-source/${token.user}/missing-source`,
          minzoom: 0,
          maxzoom: 1
        }
      }
    },
    name: 'Tileset without source'
  })
  .send()
  .then(response => {
    console.log(response)
  })
  .catch(err => {
    console.error(err)
  })
