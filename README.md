# Mapbox Tiling Service Empty Tileset Source

Sometimes you want to create a Mapbox Tileset with MTS with an empty tileset source. It could be your frontend expects the source layer in the Tileset, but today there is just no data. For example a Tileset showing active bushfires, today there may just be no fires happening.

However upon trying to create a tileset with an empty source, the Mapbox Tiling Service will respond with a 500 internal server error. This repository tests what happens in 3 scenarios, the third being a successful workaround.

## Attempt to create an empty tileset source
1. `ogr2ogr -f GeoJSONSeq empty.geojson.ld empty.geojson`
2. `./createTilesetSource.js`

Responds with 500 Internal Server Error.

## Attempt to create a tileset from a non-existent tileset source
1. `./createTilesetWithNonExistentSource.js`

Responds with 'Recipe is invalid.' 'unable to find source(s):'.

## Attempt to create a tileset source from a null geometry feature
1. `ogr2ogr -f GeoJSONSeq nullGeometry.geojson.ld nullGeometry.geojson`
2. `./createTilesetSourceFromNullGeometryFeature.js`

Successful workaround.
