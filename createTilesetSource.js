#!/usr/bin/env node

const MapboxTilesets = require('@mapbox/mapbox-sdk/services/tilesets.js')
const tilesetsService = MapboxTilesets({ accessToken: process.env.MAPBOX_ACCESS_TOKEN })

tilesetsService.createTilesetSource({
    id: 'empty-tileset-source',
    file: 'empty.geojson.ld'
  })
  .send()
  .then(response => {
    console.log(response)
  })
  .catch(err => {
    console.error(err)
  })
